package com.example.asus.music20;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ToggleButton;


public class FullActivity extends AppCompatActivity
{
    Button img;
    boolean isPlaying = false;
    float vol_left = 0.5f;
    float vol_right = 0.5f;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_full);

        // Create MediaPlayer object through which song playback is controlled
        final MediaPlayer mediaPlayer = MediaPlayer.create(this, R.raw.themoon);

        // Set default volume (0.5f, 0.5f)
        mediaPlayer.setVolume(vol_left, vol_right);

        // Play / Pause button
        ToggleButton playPause = (ToggleButton) findViewById(R.id.play);

        playPause.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View view) {
                if (isPlaying)
                {
                    mediaPlayer.pause();
                    isPlaying = false;
                }
                else
                {
                    mediaPlayer.start();
                    isPlaying = true;
                }
            }
        });

        // Volume control buttons
        // Mid
        Button volumeIncreaseMid = (Button) findViewById(R.id.volume_mid_increase);
        Button volumeDecreaseMid = (Button) findViewById(R.id.volume_mid_decrease);
        // Left
        Button volumeIncreaseLeft = (Button) findViewById(R.id.volume_left_increase);
        Button volumeDecreaseLeft = (Button) findViewById(R.id.volume_left_decrease);
        // Right
        Button volumeIncreaseRight = (Button) findViewById(R.id.volume_right_increase);
        Button volumeDecreaseRight = (Button) findViewById(R.id.volume_right_decrease);

        volumeIncreaseMid.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view) {
                if (vol_left < 1.0f) {
                    vol_left += 0.05f;
                } else {
                    vol_left = 1.0f;
                }
                if (vol_right < 1.0f) {
                    vol_right += 0.05f;
                } else {
                    vol_right = 1.0f;
                }
                mediaPlayer.setVolume(vol_left, vol_right);
            }
        });
        volumeDecreaseMid.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view) {
                if (vol_left > 0.0f) {
                    vol_left -= 0.05f;
                } else {
                    vol_left = 0.0f;
                }
                if (vol_right > 0.0f) {
                    vol_right -= 0.05f;
                } else {
                    vol_right = 0.0f;
                }
                mediaPlayer.setVolume(vol_left, vol_right);
            }
        });

        volumeIncreaseLeft.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view) {
                if (vol_left < 1.0f) {
                    vol_left += 0.05f;
                } else {
                    vol_left = 1.0f;
                }
                mediaPlayer.setVolume(vol_left, vol_right);
            }
        });
        volumeDecreaseLeft.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view) {
                if (vol_left > 0.0f) {
                    vol_left -= 0.05f;
                } else {
                    vol_left = 0.0f;
                }
                mediaPlayer.setVolume(vol_left, vol_right);
            }
        });

        volumeIncreaseRight.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view) {
                if (vol_right < 1.0f) {
                    vol_right += 0.05f;
                } else {
                    vol_right = 1.0f;
                }
                mediaPlayer.setVolume(vol_left, vol_right);
            }
        });
        volumeDecreaseRight.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view) {
                if (vol_right > 0.0f) {
                    vol_right -= 0.05f;
                } else {
                    vol_right = 0.0f;
                }
                mediaPlayer.setVolume(vol_left, vol_right);
            }
        });

        // Loop Toggle
        ToggleButton loopToggle = (ToggleButton) findViewById(R.id.loop_toggle);
        loopToggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mediaPlayer.setLooping(true);
                } else {
                    mediaPlayer.setLooping(false);
                }
            }
        });


    }
    public void Playlist(View view)
    {
        // create an intent to open the activity
        Intent playL = new Intent(this, MediaPlayerActivity.class);
        playL.putExtra("SongName", ((Button) view).getText().toString()); // sending extra data ie, song title
        //  to the next activity, so it knows what song to play!
        startActivity(playL); // open activty
    }
}
